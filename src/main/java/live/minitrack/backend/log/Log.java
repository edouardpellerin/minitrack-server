package live.minitrack.backend.log;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.UUID;
import live.minitrack.backend.model.Player;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Log {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  private UUID id;

  @ManyToOne
  @JoinColumn(name = "player_id")
  private Player player;

  @Enumerated(EnumType.STRING)
  private LogLevel level;

  private String message;

  private LocalDateTime date;
}

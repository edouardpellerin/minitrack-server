package live.minitrack.backend.log;

public record LogRequest(LogLevel level, String message, String map, String token) {}

package live.minitrack.backend.jobs;

import jakarta.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import live.minitrack.backend.map.MapRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Transactional
@RequiredArgsConstructor
public class DeleteOldMapsJob {

  private final MapRepository mapRepository;

  @Value("${minitrack.delete-permanent-minutes}")
  private int deletePositionPermanentTime = 60;

  @Scheduled(fixedRate = 1, timeUnit = TimeUnit.HOURS)
  public void scrubOldMaps() {
    mapRepository.deleteMapsOlderThan(LocalDateTime.now().minusHours(24));
  }

  @Scheduled(fixedRate = 1, timeUnit = TimeUnit.MINUTES)
  public void deleteOldPositions() {
    mapRepository.deletePositionFromPermanentMapOlderThan(
        LocalDateTime.now().minusMinutes(deletePositionPermanentTime));
  }
}

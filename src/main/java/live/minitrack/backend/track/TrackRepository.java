package live.minitrack.backend.track;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrackRepository extends JpaRepository<Track, Long> {
  List<Track> findByMapId(Long id);
}

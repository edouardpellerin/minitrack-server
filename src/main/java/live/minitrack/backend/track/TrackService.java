package live.minitrack.backend.track;

import jakarta.transaction.Transactional;
import java.util.List;
import live.minitrack.backend.map.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@Transactional
@RequiredArgsConstructor
public class TrackService {

  private final TrackRepository trackRepository;

  public List<Track> getTracksForMap(Map map) {
    return trackRepository.findByMapId(map.getId());
  }
}

package live.minitrack.backend.track;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class TrackPosition {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(targetEntity = Track.class)
  @JsonBackReference
  private Track track;

  private LocalDateTime time;

  private Double latitude;

  private Double longitude;
}

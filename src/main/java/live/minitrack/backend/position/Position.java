package live.minitrack.backend.position;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;
import live.minitrack.backend.model.Player;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString(exclude = {"player"})
public class Position {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private Double latitude;

  private Double longitude;

  private Double altitude;

  private LocalDateTime timestamp;

  private Float distance;

  private Float accuracy;

  private Float speed;

  @ManyToOne @NotNull private Player player;
}

package live.minitrack.backend.position;

import jakarta.transaction.Transactional;
import java.time.temporal.ChronoUnit;
import java.util.List;
import live.minitrack.backend.model.Player;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.springframework.stereotype.Service;

@Service
@Transactional
@RequiredArgsConstructor
public class PositionService {

  private static final int EARTH_CIRCUMFERENCE = 111320;
  private final PositionRepository positionRepository;

  public void updatePositions(Player player, List<Position> positions) {

    for (Position position : positions) {
      position.setPlayer(player);
    }
    positionRepository.saveAll(positions);
  }

  public void updateDistanceAndSpeed(Player player) {
    List<Position> positionsForPlayer =
        positionRepository.findPositionByPlayerIdOrderByTimestampAsc(player.getId());
    CircularFifoQueue<Position> lastPositions = new CircularFifoQueue<>(6);
    for (Position position : positionsForPlayer) {
      if (position.getDistance() == null) {
        // compute distance
        if (lastPositions.size() == 0) {
          position.setDistance(0f);
        } else {
          position.setDistance(
              computeDistance(position, lastPositions.get(lastPositions.size() - 1)));
        }
        positionRepository.save(position);
      }
      lastPositions.add(position);
    }

    // compute speed based on 3 latest positions
    if (lastPositions.size() > 1) {
      float distance =
          lastPositions.stream().map(Position::getDistance).reduce(0f, Float::sum)
              - lastPositions.get(0).getDistance();
      long time =
          lastPositions
              .get(0)
              .getTimestamp()
              .until(
                  lastPositions.get(lastPositions.size() - 1).getTimestamp(), ChronoUnit.SECONDS);
      float speed = distance / time * 3.6f;
      player.setInstantSpeed(speed);
    }
  }

  private float computeDistance(Position position1, Position position2) {
    double lat1 = position1.getLatitude() * EARTH_CIRCUMFERENCE;
    double lon1 =
        position1.getLongitude()
            * EARTH_CIRCUMFERENCE
            * Math.cos(position1.getLatitude() * Math.PI / 180);

    double lat2 = position2.getLatitude() * EARTH_CIRCUMFERENCE;
    double lon2 =
        position2.getLongitude()
            * EARTH_CIRCUMFERENCE
            * Math.cos(position2.getLatitude() * Math.PI / 180);

    double diffLat = Math.pow(lat1 - lat2, 2);
    double diffLon = Math.pow(lon1 - lon2, 2);
    double diffAlt = Math.pow(position1.getAltitude() - position2.getAltitude(), 2);

    return (float) Math.sqrt(diffLat + diffLon + diffAlt);
  }
}

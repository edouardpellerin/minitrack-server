package live.minitrack.backend.web;

import java.util.Locale;
import live.minitrack.backend.web.exception.MapNotFoundException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
@Slf4j
public class ExceptionController {

  private MessageSource messageSource;

  public ExceptionController(MessageSource messageSource) {
    this.messageSource = messageSource;
  }

  @ExceptionHandler({MapNotFoundException.class})
  public ModelAndView mapNotFound(Exception exception, Locale locale) {
    log.error(exception.getMessage(), exception);
    ModelAndView modelAndView = new ModelAndView();
    Error error = new Error();
    error.setTitle(messageSource.getMessage("error.map-not-found.title", null, locale));
    error.setMessage(messageSource.getMessage("error.map-not-found.message", null, locale));
    modelAndView.addObject("mapError", error);
    modelAndView.setViewName("map-error");
    return modelAndView;
  }

  @Getter
  @Setter
  private static class Error {
    private String title;
    private String message;
  }
}

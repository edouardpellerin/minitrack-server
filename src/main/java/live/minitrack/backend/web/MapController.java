package live.minitrack.backend.web;

import java.util.Optional;
import live.minitrack.backend.map.Map;
import live.minitrack.backend.map.MapRepository;
import live.minitrack.backend.web.exception.MapNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@RequiredArgsConstructor
public class MapController {

  private final MapRepository mapRepository;

  @Value("${mapbox.token}")
  private String mapboxToken;

  @GetMapping(value = "/map/{key}")
  public String showMap(@PathVariable String key, Model model) throws MapNotFoundException {
    Optional<Map> map = mapRepository.findByKey(key);
    if (map.isPresent()) {
      model.addAttribute("map", map.get());
      model.addAttribute("centerButtonVisible", true);
      model.addAttribute("title", map.get().getTitle());
      model.addAttribute("mapboxToken", mapboxToken);
      return "map";
    } else {
      throw new MapNotFoundException();
    }
  }

  @GetMapping(value = "/map/{key}/webview")
  public String showMapWebview(@PathVariable String key, Model model) throws MapNotFoundException {
    Optional<Map> map = mapRepository.findByKey(key);
    if (map.isPresent()) {
      model.addAttribute("map", map.get());
      model.addAttribute("centerButtonVisible", true);
      model.addAttribute("title", map.get().getTitle());
      model.addAttribute("mapboxToken", mapboxToken);
      return "map-webview";
    } else {
      throw new MapNotFoundException();
    }
  }
}

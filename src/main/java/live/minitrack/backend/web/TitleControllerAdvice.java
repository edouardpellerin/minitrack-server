package live.minitrack.backend.web;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class TitleControllerAdvice {
  @ModelAttribute("title")
  public String getTitle() {
    return "MiniTrack";
  }
}

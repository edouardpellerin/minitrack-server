package live.minitrack.backend.model;

import jakarta.persistence.*;
import live.minitrack.backend.map.Map;
import lombok.*;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "map")
public class Player {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @ManyToOne private Map map;

  private String name;

  private String token;

  private Float instantSpeed;
}

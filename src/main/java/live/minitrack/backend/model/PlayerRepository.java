package live.minitrack.backend.model;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<Player, Long> {
  Optional<Player> findByMapKeyAndToken(String key, String token);
}

package live.minitrack.backend.model;

public enum PointOfInterestType {
  START, STOP, WAYPOINT
}

package live.minitrack.backend.map;

import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface MapRepository extends JpaRepository<Map, Long> {
  Optional<Map> findByKey(String key);

  Optional<Map> findBySharingCode(String code);

  @Modifying
  @Query(
      value = "delete from map where creation_date < ?1 and permanent = false ",
      nativeQuery = true)
  void deleteMapsOlderThan(LocalDateTime olderThan);

  @Modifying
  @Query(
      value =
          "delete from position where position.timestamp < ?1 and player_id in (select p.id from player p join map m on p.map_id = m.id where m.permanent = true)",
      nativeQuery = true)
  void deletePositionFromPermanentMapOlderThan(LocalDateTime olderThan);
}

package live.minitrack.backend.map;

import io.jenetics.jpx.GPX;
import io.jenetics.jpx.TrackSegment;
import io.jenetics.jpx.WayPoint;
import jakarta.transaction.Transactional;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import live.minitrack.backend.model.Player;
import live.minitrack.backend.model.PlayerRepository;
import live.minitrack.backend.track.Track;
import live.minitrack.backend.track.TrackPosition;
import live.minitrack.backend.track.TrackPositionRepository;
import live.minitrack.backend.track.TrackRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@Transactional
@RequiredArgsConstructor
public class MapService {

  private final TrackPositionRepository trackPositionRepository;
  private final MapRepository mapRepository;
  private final PlayerRepository playerRepository;
  private final TrackRepository trackRepository;
  private final SecureRandom random = new SecureRandom();

  public void addTrack(String key, String gpx) {
    Optional<Map> map = mapRepository.findByKey(key);

    if (map.isPresent()) {

      Track track = new Track();
      track.setMap(map.get());

      List<WayPoint> wayPoints =
          GPX.reader()
              .fromString(gpx)
              .tracks()
              .flatMap(io.jenetics.jpx.Track::segments)
              .flatMap(TrackSegment::points)
              .collect(Collectors.toList());

      List<TrackPosition> trackPositionList = new ArrayList<>();

      for (WayPoint wayPoint : wayPoints) {
        TrackPosition trackPosition = new TrackPosition();
        trackPosition.setLatitude(wayPoint.getLatitude().doubleValue());
        trackPosition.setLongitude(wayPoint.getLongitude().doubleValue());
        trackPosition.setTrack(track);
        if (wayPoint.getTime().isPresent()) {
          trackPosition.setTime(wayPoint.getTime().get().toLocalDateTime());
        }
        trackPositionList.add(trackPosition);
      }
      trackRepository.save(track);
      trackPositionRepository.saveAll(trackPositionList);
    }
  }

  public MapCreation createMap(String name, String map2) {
    String key = new BigInteger(40, random).toString(32);
    Map map = Map.builder().title(map2).key(key).creationDate(ZonedDateTime.now()).build();
    map = mapRepository.save(map);
    Player player = new Player();
    player.setName(name);
    player.setMap(map);
    player.setToken(new BigInteger(40, random).toString(32));
    playerRepository.save(player);
    return new MapCreation(map, player);
  }
}

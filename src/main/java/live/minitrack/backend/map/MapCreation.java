package live.minitrack.backend.map;

import live.minitrack.backend.model.Player;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class MapCreation {
  private Map map;
  private Player player;
}

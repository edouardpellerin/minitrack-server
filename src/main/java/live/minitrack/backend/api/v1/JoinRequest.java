package live.minitrack.backend.api.v1;

import lombok.Data;

@Data
class JoinRequest {
  private String name;
  private String code;
}

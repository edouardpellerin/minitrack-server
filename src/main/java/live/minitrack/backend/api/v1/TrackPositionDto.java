package live.minitrack.backend.api.v1;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrackPositionDto {
  Double latitude;
  Double longitude;
}

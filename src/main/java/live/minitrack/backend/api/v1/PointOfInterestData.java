package live.minitrack.backend.api.v1;

import live.minitrack.backend.model.PointOfInterestType;
import lombok.Data;

@Data
public class PointOfInterestData {
  private double[] position;
  private String description;
  private PointOfInterestType type;
}

package live.minitrack.backend.api.v1;

import live.minitrack.backend.position.Position;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
interface PositionMapper {
  @Mappings({
    @Mapping(target = "player", ignore = true),
    @Mapping(target = "id", ignore = true),
    @Mapping(target = "distance", ignore = true)
  })
  Position map(PlayerPosition position);
}

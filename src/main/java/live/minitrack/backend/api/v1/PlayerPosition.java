package live.minitrack.backend.api.v1;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import lombok.Value;

@Value
class PlayerPosition {
  private double longitude;
  private double latitude;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
  private LocalDateTime timestamp;

  private double altitude;
  private float accuracy;
  private float speed;
}

package live.minitrack.backend.api.v1;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrackDto {
  private List<Double[]> positions;
}

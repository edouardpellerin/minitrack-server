package live.minitrack.backend.api.v1;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
class PlayerUpdate {
  private String playerToken;
  private List<PlayerPosition> positions;
}

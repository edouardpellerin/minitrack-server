package live.minitrack.backend.api.v1;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MapResponse {
  private String title;
  private String key;
}

package live.minitrack.backend.api.v1;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JoinResponse {
  private String name;
  private String playerToken;
  private MapResponse map;
}

CREATE TABLE point_of_interest
(
  id          bigserial primary key,
  description text   not null,
  latitude    decimal(8, 5) not null,
  longitude   decimal(8, 5) not null,
  map_id      bigint not null,
  constraint fk_point_of_interest_map foreign key (map_id) references map(id) on delete cascade
);


alter table map add column multiplayer boolean not null default false;

create table player (id bigserial, map_id bigint not null, name text not null, private_token text not null);

ALTER TABLE  player ADD CONSTRAINT pk_player PRIMARY KEY (id);

ALTER TABLE player ADD CONSTRAINT fk_player_map FOREIGN KEY (map_id) references map(id) ON DELETE CASCADE;

CREATE INDEX fk_player_map ON player(map_id);

ALTER TABLE position ADD COLUMN player_id bigint;
ALTER TABLE position ADD CONSTRAINT fk_position_player FOREIGN KEY (player_id) REFERENCES player(id) ON DELETE CASCADE;

CREATE INDEX fk_position_player ON position(player_id)
CREATE TABLE track_position
(
    id        bigserial primary key,
    map_id    bigint not null,
    latitude  double precision,
    longitude double precision
);

ALTER TABLE track_position
    ADD CONSTRAINT track_position_map_fk FOREIGN KEY (map_id) REFERENCES map (id) ON DELETE CASCADE;
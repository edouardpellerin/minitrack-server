create table map (
	id bigint not null,
	"key" text not null unique,
	title text not null,
	update_token text not null,
	constraint pk_map primary key (id)
);

CREATE SEQUENCE seq_map_id START 101;


create table position (
	id bigint not null,
	latitude decimal(8,5),
	longitude decimal(8,5),
	"timestamp" timestamp,
	map_id bigint,
	constraint pk_position primary key (id),
	constraint fk_position_map foreign key (map_id) references map(id)
);

CREATE SEQUENCE seq_position_id START 101;
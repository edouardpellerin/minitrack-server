ALTER TABLE map ALTER COLUMN id SET DEFAULT nextval('seq_map_id');
ALTER TABLE position ALTER COLUMN id SET DEFAULT nextval('seq_position_id');

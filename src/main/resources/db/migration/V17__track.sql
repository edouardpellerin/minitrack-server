create table track
(
    id     bigserial primary key,
    map_id bigint not null references map (id) on delete cascade
);

alter table track_position
    drop column map_id;

alter table track_position
    add column track_id bigint references track (id) on delete cascade;

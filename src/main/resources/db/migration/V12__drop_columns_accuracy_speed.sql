ALTER TABLE "position" DROP COLUMN accuracy;
ALTER TABLE "position" DROP COLUMN speed;

ALTER TABLE "position" alter COLUMN distance type double precision;